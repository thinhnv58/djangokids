from django import forms

class ContactForm(forms.Form):
	email = forms.EmailField()
	full_name = forms.CharField(required=False)
	message = forms.CharField()
	