from django.conf.urls import url, include


app_name='core'
urlpatterns = [
	url(r'^$', 'core.views.home', name='home'),
	url(r'^about/$', 'core.views.about', name='about'),
]
