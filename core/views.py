from django.shortcuts import render

from tutorial.models import Tutorial
from blog.models import Story
from event.models import Event

from .forms import ContactForm
def home(request):
	tutorials = Tutorial.objects.filter(draft=False).order_by('-publish')[:3]
	stories = Story.objects.filter(draft=False).order_by('timestamp')[:5]
	stories2 = Story.objects.filter(draft=False).order_by('timestamp')[:2]
	
	events = Event.objects.all()
	context = {
		'tutorials':tutorials,
		'stories': stories,
		'events':events,
	}	
	return render(request, 'home.html', context)


def about(request):
	form = ContactForm(request.POST or None)
	context = {
		'form': form,
	}
	if form.is_valid():
		email = form.cleaned_data.get('email')
		full_name = form.cleaned_data.get('full_name')
		message = form.cleaned_data.get('message')
		import smtplib
		fromaddr = 'vnpython.org@gmail.com'
		toaddrs  = 'thinhnv.58@gmail.com'
		msg = email + " send "+ message


		# Credentials (if needed)
		username = 'vnpython.org'
		password = 'vnpython1'

		# The actual mail send
		server = smtplib.SMTP('smtp.gmail.com:587')
		server.ehlo()
		server.starttls()
		server.login(username,password)
		server.sendmail(fromaddr, toaddrs, msg)
		server.quit()



	return render(request, 'about.html', context)