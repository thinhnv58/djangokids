from django.db import models


from django.conf import settings
from django.utils import timezone	
from django.core.urlresolvers import reverse
from django.db.models.signals import pre_save
from django.utils.text import slugify

class Topic(models.Model):
	title = models.CharField(max_length=120)
	description = models.CharField(max_length=120, blank=True, null=True)
	photo = models.CharField(max_length=200, null=True, blank=True)
	def __str__(self):
		return self.title


class Story (models.Model):
	user = models.ForeignKey(settings.AUTH_USER_MODEL, default=1)
	topic = models.ForeignKey(Topic, on_delete=models.CASCADE)
	title = models.CharField(max_length=200)
	content = models.TextField()
	slug = models.SlugField(unique=True, blank=True)

	photo = models.CharField(max_length=200, blank=True, null=True)
	publish = models.DateTimeField()
	timestamp = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)
	draft = models.BooleanField(default=False)
	
	def __str__(self):
		return self.title
	def get_absolute_url(self):
		return reverse("blog:story_detail", kwargs={"slug": self.slug})



def create_slug(instance, new_slug=None):
    slug = slugify(instance.title)
    if new_slug is not None:
        slug = new_slug
    qs = Story.objects.filter(slug=slug).order_by("-id")
    exists = qs.exists()
    if exists:
        new_slug = "%s-%s" %(slug, qs.first().id)
        return create_slug(instance, new_slug=new_slug)
    return slug


def pre_save_story_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = create_slug(instance)


pre_save.connect(pre_save_story_receiver, sender=Story)