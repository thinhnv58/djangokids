from django.contrib import admin


from .models import Topic, Story
# Register your models here.

admin.site.register(Topic)
admin.site.register(Story)
