from django import forms

from .models import Story
from pagedown.widgets   import PagedownWidget
class StoryForm(forms.ModelForm):
	content = forms.CharField(widget=PagedownWidget)
	publish = forms.DateField(widget=forms.SelectDateWidget)
	class Meta:
		model = Story
		fields = ['topic', 'title', 'content', 'photo', 'publish', 'draft']
		


