from django.conf.urls import url

app_name='blog'
urlpatterns = [
	url(r'^$', 'blog.views.story_list', name='story_list'),
	url(r'^createstory', 'blog.views.story_create'),
	url(r'^(?P<slug>[\w-]+)/$', 'blog.views.story_detail', name='story_detail'),
	url(r'^(?P<slug>[\w-]+)/edit$', 'blog.views.story_update', name='story_update'),
	url(r'^(?P<slug>[\w-]+)/delete$', 'blog.views.story_delete', name='story_delete'),

]