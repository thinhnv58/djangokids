from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseRedirect, Http404

from .models import Story
from .forms import StoryForm

# Create your views here.


def story_list(request):
	stories = Story.objects.all()
	context = {
		'stories': stories,
	}
	return render(request, 'blog/story_list.html', context)

def story_create(request):
	if not request.user.is_staff or not request.user.is_superuser:
		raise Http404
	
	form = StoryForm(request.POST or None, request.FILES or None)

	if form.is_valid():
		instance = form.save(commit=False)
		instance.save()
		return HttpResponseRedirect(instance.get_absolute_url())
	context = {
		'form': form,
	}
	return render(request, 'blog/story_form.html', context)

def story_detail(request, slug=None):
	instance = get_object_or_404(Story, slug=slug)
	context = {
		'instance': instance
	}
	return render(request, 'blog/story_detail.html', context)


def story_update(request, slug=None):
	if not request.user.is_staff or not request.user.is_superuser:
		raise Http404
	
	instance = get_object_or_404(Story, slug=slug)
	form = StoryForm(request.POST or None, request.FILES or None, instance=instance)

	if form.is_valid():
		instance = form.save(commit=False)
		instance.save()
		return HttpResponseRedirect(instance.get_absolute_url())
	context ={
		'form':form,
		'instance': instance,
	}
	return render(request, 'blog/story_form.html', context)

def story_delete(request, slug):
	if not request.user.is_staff or not request.user.is_superuser:
		raise Http404
	
	instance = get_object_or_404(Story, slug=slug)
	instance.delete()
	return redirect("blog:story_list")

