from django import forms


from pagedown.widgets import PagedownWidget
from .models import Tutorial, Lesson

class TutorialForm(forms.ModelForm):
	publish = forms.DateField(widget=forms.SelectDateWidget)
	class Meta:
		model = Tutorial
		fields = [ 'title', 'photo', 'description', 'publish', 'draft']

class LessonForm(forms.ModelForm):
	content = forms.CharField(widget=PagedownWidget)

	class Meta:
		model = Lesson
		fields = [ 'tutorial', 'title', 'content', 'draft' ]

