from django.db import models

from django.conf import settings
from django.utils import timezone	
from django.core.urlresolvers import reverse
from django.db.models.signals import pre_save
from django.utils.text import slugify
# Create your models here.

class Tutorial(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, default=1)
    title = models.CharField(max_length=120)
    photo = models.CharField(max_length=200)	
    slug = models.SlugField(unique=True, blank=True)
    description = models.CharField(max_length=200)
    publish = models.DateTimeField()
    draft = models.BooleanField(default=False)
    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("tutorial:lesson_list", kwargs={"tutorial_slug": self.slug})


class Lesson(models.Model):
    tutorial = models.ForeignKey(Tutorial, on_delete=models.CASCADE)
    title = models.CharField(max_length=120)
    slug = models.SlugField(unique=True, blank=True)
    content = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    draft = models.BooleanField(default=False)
    def __str__(self):
        return self.title
    def get_absolute_url(self):
        return reverse("tutorial:lesson_detail", kwargs={"tutorial_slug": self.tutorial.slug, "lesson_slug":self.slug})

    class Meta:
        ordering = ["-timestamp", "-updated"]


def create_tutorial_slug(instance, new_slug=None):
    slug = slugify(instance.title)
    if new_slug is not None:
        slug = new_slug
    qs = Tutorial.objects.filter(slug=slug).order_by("-id")
    exists = qs.exists()
    if exists:
        new_slug = "%s-%s" %(slug, qs.first().id)
        return create_tutorial_slug(instance, new_slug=new_slug)
    return slug

def create_lesson_slug(instance, new_slug=None):
    slug = slugify(instance.title)
    if new_slug is not None:
        slug = new_slug
    qs = Lesson.objects.filter(slug=slug).order_by("-id")
    exists = qs.exists()
    if exists:
        new_slug = "%s-%s" %(slug, qs.first().id)
        return create_lesson_slug(instance, new_slug=new_slug)
    return slug




def pre_save_tutorial_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = create_tutorial_slug(instance)
def pre_save_lesson_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = create_lesson_slug(instance)



pre_save.connect(pre_save_tutorial_receiver, sender=Tutorial)
pre_save.connect(pre_save_lesson_receiver, sender=Lesson)
