from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseRedirect, Http404


from .models import Tutorial, Lesson
from .forms import TutorialForm, LessonForm

##### TUTORIAL  ######

def tutorial_create(request):
	if not request.user.is_staff or not request.user.is_superuser:
		raise Http404
	
	form = TutorialForm(request.POST or None, request.FILES or None)
	if form.is_valid():
		instance = form.save(commit=False)
		instance.save()
		return HttpResponseRedirect(instance.get_absolute_url())

	context = {
		'form': form, 
	}
	return render(request, 'tutorial/tutorial_form.html', context)

# def tutorial_detail(request, tutorial_slug):
# 	instance = get_object_or_404(Tutorial, slug=tutorial_slug)
# 	context = {
# 		'instance': instance,
# 	}
# 	return render(request, 'tutorial/tutorial_detail.html', context)

def tutorial_list(request):
	tutorials = Tutorial.objects.all()

	context = {
		'tutorials': tutorials,
	}
	return render(request, 'tutorial/tutorial_list.html', context)

def tutorial_update(request, tutorial_slug):
	if not request.user.is_staff or not request.user.is_superuser:
		raise Http404
	
	instance = get_object_or_404(Tutorial, slug=tutorial_slug)
	form = TutorialForm(request.POST or None, request.FILES or None, instance=instance)
	if form.is_valid():
		instance = form.save(commit=False)
		instance.save()
		return HttpResponseRedirect(instance.get_absolute_url())
	context = {
		'form': form,
		'instance': instance,
	}
	return render(request, 'tutorial/tutorial_form.html', context)

def tutorial_delete(request, tutorial_slug):
	if not request.user.is_staff or not request.user.is_superuser:
		raise Http404
	
	instance = get_object_or_404(Tutorial, slug=tutorial_slug)
	instance.delete()
	return redirect("tutorial:tutorial_list")





####### LESSON #######

def lesson_list(request, tutorial_slug):
	tutorial = get_object_or_404(Tutorial, slug=tutorial_slug)
	lessons = tutorial.lesson_set.all()
	lesson_first = lessons
	for lesson in lessons:
		lesson_first=lesson
		break


	context = {
		'tutorial':tutorial,
		'lessons':lessons,
		'lesson_first':lesson_first,
	}
	if tutorial_slug == 'hello-django':
		context = {		
			'tutorial':tutorial,
			'lessons':lessons,
			'lesson_first':lesson_first,
			'is_hello_django': True,
		}
	return render(request, 'tutorial/lesson_list.html', context)

def lesson_create(request, tutorial_slug):
	if not request.user.is_staff or not request.user.is_superuser:
		raise Http404
	
	form = LessonForm (request.POST or None)

	context = {
		'form': form,
	}
	if form.is_valid():
		instance = form.save(commit=False)
		instance.save()
		return HttpResponseRedirect(instance.get_absolute_url())

	return render(request, 'tutorial/lesson_form.html', context)

def lesson_detail(request, tutorial_slug, lesson_slug):
	instance = get_object_or_404(Lesson, slug=lesson_slug)
	list_lesson_in_tutorial = Lesson.objects.filter(tutorial=instance.tutorial)
	context = {
		'instance': instance,
		'list_lesson_in_tutorial': list_lesson_in_tutorial,
	}
	return render(request, 'tutorial/lesson_detail.html', context)

def lesson_update(request, tutorial_slug, lesson_slug):
	if not request.user.is_staff or not request.user.is_superuser:
		raise Http404
	
	instance = get_object_or_404(Lesson, slug=lesson_slug)
	form = LessonForm(request.POST or None, instance=instance)
	context = {
		'instance': instance,
		'form': form,
	}
	if form.is_valid():
		instance = form.save(commit=False)
		instance.save()
		return HttpResponseRedirect(instance.get_absolute_url())

	return render(request, 'tutorial/lesson_form.html', context)
	
def lesson_delete(request, tutorial_slug, lesson_slug):
	if not request.user.is_staff or not request.user.is_superuser:
		raise Http404
	
	instance = get_object_or_404(Lesson, slug=lesson_slug)
	instance.delete()
	return redirect("tutorial:lesson_list", tutorial_slug=tutorial_slug)


