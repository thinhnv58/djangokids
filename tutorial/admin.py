from django.contrib import admin

from .models import Tutorial, Lesson
# Register your models here.
admin.site.register(Tutorial)
admin.site.register(Lesson)