from django.conf.urls import url

app_name="tutorial"
urlpatterns = [
	url(r'^$', 'tutorial.views.tutorial_list', name='tutorial_list'),
	url(r'^createtutorial/$', 'tutorial.views.tutorial_create', name='tutorial_create'),
	url(r'^(?P<tutorial_slug>[\w-]+)/edit/$', 'tutorial.views.tutorial_update', name='tutorial_update'),
	url(r'^(?P<tutorial_slug>[\w-]+)/delete/$', 'tutorial.views.tutorial_delete', name='tutorial_delete'),	
	
	url(r'^(?P<tutorial_slug>[\w-]+)/$', 'tutorial.views.lesson_list', name='lesson_list'),
	url(r'^(?P<tutorial_slug>[\w-]+)/createlesson/$', 'tutorial.views.lesson_create'),
	url(r'^(?P<tutorial_slug>[\w-]+)/(?P<lesson_slug>[\w-]+)/$', 'tutorial.views.lesson_detail', name='lesson_detail'),
	url(r'^(?P<tutorial_slug>[\w-]+)/(?P<lesson_slug>[\w-]+)/edit/$', 'tutorial.views.lesson_update', name='lesson_update'),
	url(r'^(?P<tutorial_slug>[\w-]+)/(?P<lesson_slug>[\w-]+)/delete/$', 'tutorial.views.lesson_delete', name='lesson_delete' ),
]