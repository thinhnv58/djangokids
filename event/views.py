from django.shortcuts import render

from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseRedirect, Http404

from .models import Event
# Create your views here.
def event_list(request):
	events = Event.objects.all()
	context = {
		'events': events,
	}
	return render(request, 'event/event_list.html', context)


def event_detail(request, slug):
	instance = get_object_or_404(Event, slug=slug)
	context = {
		'instance':instance,
	}
	return render(request, 'event/event_detail.html', context)

