from django.db import models

from django.conf import settings
from django.utils import timezone	
from django.core.urlresolvers import reverse
from django.db.models.signals import pre_save
from django.utils.text import slugify

# Create your models here.
class Event (models.Model):
	name = models.CharField(max_length=120)
	description = models.CharField(max_length=200)
	slug = models.SlugField(unique=True, blank=True)
	information = models.TextField()
	photo = models.CharField(max_length=200)
	cover = models.CharField(max_length=200, blank=True, null=True)
    #publish = models.DateTimeField()
	def __str__(self):
		return self.name

	def get_absolute_url(self):
		return reverse("event:event_detail", kwargs={"slug": self.slug})




def create_slug(instance, new_slug=None):
    slug = slugify(instance.name)
    if new_slug is not None:
        slug = new_slug
    qs = Event.objects.filter(slug=slug).order_by("-id")
    exists = qs.exists()
    if exists:
        new_slug = "%s-%s" %(slug, qs.first().id)
        return create_slug(instance, new_slug=new_slug)
    return slug


def pre_save_event_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = create_slug(instance)


pre_save.connect(pre_save_event_receiver, sender=Event)