from django.conf.urls import url

app_name='event'
urlpatterns = [
	url(r'^$', 'event.views.event_list', name='event_list'),
	url(r'^(?P<slug>[\w-]+)/$', 'event.views.event_detail', name='event_detail'),	
]
